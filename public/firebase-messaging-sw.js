importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js');



const firebaseConfig = {
  apiKey: "AIzaSyCS61kx84zrmk2wlotJDWq6Yb7fKTHqjZ0",
  authDomain: "pushnotificationzeo.firebaseapp.com",
  projectId: "pushnotificationzeo",
  storageBucket: "pushnotificationzeo.appspot.com",
  messagingSenderId: "1049683037344",
  appId: "1:1049683037344:web:db2b6120a54e9145750dc5",
  measurementId: "G-QJ129S0T09"
};

firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();

messaging.onBackgroundMessage(function(payload) {
    console.log('Received background message ', payload);

    const notificationTitle = payload.notification.title;
    const notificationOptions = {
    body: payload.notification.body,
    };

    self.registration.showNotification(notificationTitle,
    notificationOptions);
});



