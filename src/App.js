import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom'
import {Header} from './Components/Navbar/Navbar.js'
import {Contact} from './pages/Contact.js'
import { Home } from './pages/Home.js';
import React from 'react';
import {FirebaseApp} from './firebase/firebase.js';
import {getTokenFirebase} from './firebase/firebase'
getTokenFirebase()



console.log(FirebaseApp);

export const App = ()=> {

  return (
    <BrowserRouter>
    <Header/>
      <Switch>
        <Route exact path = '/' component={Home}/>
        <Route exact  path = '/Contact' component={Contact}/>
        <Redirect to='/'/>
      </Switch>
    </BrowserRouter>
  );
  }