import styled from "styled-components"
import {Link} from 'react-router-dom'
import { AiFillAndroid } from "react-icons/ai";
import { FiList,FiMinimize2 } from "react-icons/fi";
import { useState } from "react";


const NavbarContainer = styled.div`
    width: 100%;
    height: 10vh;
    position: sticky;
    top: 10;
    z-index:99;
    background-color: #FFFF;
`
const NavbarWrapper = styled.div`
    margin: auto;
    width:100%;
    max-width: 1300px;
    height:100%;
    align-items: center;
    display:flex;
    flex-wrap: wrap;
    justify-content: space-between;
    border-radius: 8px ;
    background-color: blue;
`
const IconLogo = styled.div`
    display:flex;
    flex-wrap:wrap;
    justify-content:flex-start;
    align-items: center;
    font-family:"Zeo";
    color: yellow;
`

const Menu = styled.ul`
    height:100%auto;
    display:flex;
    justify-content:center;
    align-items:center;

    @media screen and (max-width: 960px){
        width:100%;
        height:98vh;
        position: absolute;
        top: 88px;
        left: ${({click})=> (click ? 10: "-100%")};
        flex-direction: 0.5s all ease-inherit;
        background-color: #49626c;
    }
`

const MenuItem = styled.li`
    height:100%;
    padding: 0.5rem 1.5rem;
    display: flex;
    justify-content:center;
    align-items: center;
    font-size: 1.2rem;
    font-family:"Zeo";
    font-weight: 400;
&:hover{
    background-color:#343257;
    border-bottom: 0.3rem solid #EBC88B;
    transition: 0.4s ease-in;
}

@media screen and (max-width: 960px)
{
    width: 100%;
    height:78px;
}
`

const MenuItemLink = styled.a`
    text-decoration:none;
    color: #EBC88B;
`

const IconLogoMobile = styled.div`
display:none;
@media screen and (max-width: 960px){
    display:flex;
    color:#EBC88B;
    font-size: 2rem;
}
`

export const Header = () => {

    const [click,setClick] = useState(false);

    const ChangeClick = ()=> {
        setClick(!click);
    }
    return (
        <>
            <NavbarContainer>
                <NavbarWrapper>
                    <IconLogo>
                        <AiFillAndroid size={"3em"}/>
                    </IconLogo>
                    <IconLogoMobile onClick = {()=>ChangeClick()}>
                        {click ? <FiMinimize2/>: <FiList/>}
                    </IconLogoMobile>
                    <Menu clcik>
                        <MenuItem onClick = {()=>ChangeClick()}>
                            <MenuItemLink><Link to='/'>Home</Link> </MenuItemLink>
                        </MenuItem>
                        <MenuItem onClick = {()=>ChangeClick()}>
                            <MenuItemLink>ABOUT US </MenuItemLink>
                        </MenuItem>
                        <MenuItem onClick = {()=>ChangeClick()}>
                            <MenuItemLink> <Link  to='/Contact'>Contact</Link>  </MenuItemLink>
                        </MenuItem>
                        
                    </Menu>
                </NavbarWrapper>
            </NavbarContainer>
        </>
        
    )
}