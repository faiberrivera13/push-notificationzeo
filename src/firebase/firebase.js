import { initializeApp } from "firebase/app";
import { getMessaging, getToken,onMessage} from "firebase/messaging";

const firebaseConfig = {
    apiKey: "AIzaSyCS61kx84zrmk2wlotJDWq6Yb7fKTHqjZ0",
    authDomain: "pushnotificationzeo.firebaseapp.com",
    projectId: "pushnotificationzeo",
    storageBucket: "pushnotificationzeo.appspot.com",
    messagingSenderId: "1049683037344",
    appId: "1:1049683037344:web:db2b6120a54e9145750dc5",
    measurementId: "G-QJ129S0T09"
};

export const FirebaseApp = initializeApp(firebaseConfig);
export const messaging = getMessaging();


const {REACT_APP_VAPID_KEY} = process.env
const publicKey = REACT_APP_VAPID_KEY

// Get Token
export const getTokenFirebase = async()=>{
    let currentToken = ''
    try {
        currentToken = await getToken(messaging, {vapidKey:"BGHNWXP-6hHuiBzAmqLtY1gWlp3gKHfyqDpY0HF4MjrdVrDzdl1IaFaduaRRdPt817lIEg9BQvVxqyR47fUAqDQ"})
        if(currentToken){
            console.warn('success token',currentToken)
        } else {
            console.warn('failed token',currentToken)
        }
    } catch (error) {
        console.log('Ocurrio un error obteniendo el token', error)
    }
    return currentToken
}
// On Message
export const firstPlaneMessage = onMessage(messaging, (payload)=>{
    console.log('Message received. ', payload)
})
export const onMessageListener = () =>
    new Promise((resolve) => {
    messaging.onMessage((payload) => {
    resolve(payload);
    });
});
