import { useState } from 'react';
import styled from "styled-components"

import { Container } from '../Components/Home/Container';
import { Text } from '../Components/Home/Text';

const Buttom = styled.button`
        border: none;
        border-radius: 2em;
        background-color: peru;
        cursor: pointer;
`

export const Home = () => {
    const [count,setCount] = useState(0)
   // const [name, setName]= useState('')
    return (
        <Container>
            <Text 
                text={count}/>
            
            <Buttom onClick={()=> setCount(count+1)}>
            <Text 
                text='Click Here'
                  color='red'
                  fontSize='10em'/>
            </Buttom>   
        </Container>
    )
}